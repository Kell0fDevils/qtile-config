# -----------------------------------------------------------------
#   _   __     _ _   _____  __  ______           _ _     
#  | | / /    | | | |  _  |/ _| |  _  \         (_) |    
#  | |/ /  ___| | | | | | | |_  | | | |_____   ___| |___ 
#  |    \ / _ \ | | | | | |  _| | | | / _ \ \ / / | / __|
#  | |\  \  __/ | | \ \_/ / |   | |/ /  __/\ V /| | \__ \
#  \_| \_/\___|_|_|  \___/|_|   |___/ \___| \_/ |_|_|___/
# -----------------------------------------------------------------
# Copyright (c) 2010 Aldo Cortesi
# Copyright (c) 2010, 2014 dequis
# Copyright (c) 2012 Randall Ma
# Copyright (c) 2012-2014 Tycho Andersen
# Copyright (c) 2012 Craig Barnes
# Copyright (c) 2013 horsik
# Copyright (c) 2013 Tao Sauvage
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from typing import List  # noqa: F401

import os
import re
import socket
import subprocess
from libqtile import bar, layout, widget, hook
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

mod = "mod4"
mod1 = "alt"
mod2 = "control"
terminal = guess_terminal()

keys = [
    # Terminals
    Key([mod], "t", lazy.spawn('qterminal')),
    Key([mod, "shift"], "Return", lazy.spawn('urxvt')),
    Key([mod], "Return", lazy.spawn('st')),

    # Apps Meta + Key only
    Key([mod], "a", lazy.spawn('pavucontrol-qt')),
    Key([mod], "b", lazy.spawn('/home/kell/App-Images/Browser/Brave-Nightly.AppImage')),
    Key([mod], "e", lazy.spawn('atom')),
    Key([mod], "f", lazy.spawn('pcmanfm-qt')),
    Key([mod], "g", lazy.spawn('flatpak run com.axosoft.GitKraken')),
    Key([mod], "h", lazy.spawn('urxvt -e htop')),
    Key([mod], "r", lazy.spawn("dmenu_run -i -nb '#17161c' -nf '#372e5e' -sb '#372e5e' -sf '#17161c' -fn 'Hack:bold:pixelsize=18'")),
    Key([mod], "s", lazy.spawn('steam')),
    Key([mod], "v", lazy.spawn('vlc')),
    Key([mod], "p", lazy.spawn('flameshot')),
    Key([mod], "o", lazy.spawn('obs')),
    Key([mod], "k", lazy.spawn('killall picom')),

    # Apps Meta + Mod1 + Key Only
    Key([mod, "mod1"], "b", lazy.spawn('/home/kell/App-Images/Browser/Firefox-Nightly.AppImage')),
    Key([mod, "mod1"], "f", lazy.spawn('nemo')),
    Key([mod, "mod1"], "p", lazy.spawn('picom --vsync')),
    Key([mod, "mod1"], "g", lazy.spawn('gimp')),
    Key([mod, "mod1"], "k", lazy.spawn('kdenlive')),
    Key([mod, "mod1"], "s", lazy.spawn('flatpak run com.spotify.Client')),

    # Apps Meta + Mod2 + Key Only


    # Apps Meta + Shift + Key Only


    # Apps Alt + Key only


    # Manages Logout/Shutdown/Sleep
    Key([mod, "shift"], "e", lazy.spawn('qtileshutdownmenu')),

    # Moves the focus to another window.
    Key([mod], "Left", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "Right", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "Down", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "Up", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(),
        desc="Move window focus to other window"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "Left", lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"], "Right", lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"], "Down", lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "Up", lazy.layout.shuffle_up(), desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "Left", lazy.layout.grow_left(),
        desc="Grow window to the left"),
    Key([mod, "control"], "Right", lazy.layout.grow_right(),
        desc="Grow window to the right"),
    Key([mod, "control"], "Down", lazy.layout.grow_down(),
        desc="Grow window down"),
    Key([mod, "control"], "Up", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key(["mod1", "shift"], "Return", lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack"),
    #Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),

    # Toggle between different layouts as defined below
    Key([mod, "mod1"], "space", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod], "q", lazy.window.kill(), desc="Kill focused window"),

    Key([mod, "control"], "r", lazy.restart(), desc="Restart Qtile"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
   # Key([mod], "r", lazy.spawncmd(),
    #    desc="Spawn a command using a prompt widget"),
]
groups = [Group(i) for i in "123456789"]

for i in groups:
    keys.extend([

#CHANGE WORKSPACES
        Key([mod], i.name, lazy.group[i.name].toscreen()),
        Key(["mod1"], "j", lazy.screen.next_group()),
        Key(["mod1"], "l", lazy.screen.prev_group()),
        Key(["mod1"], "Tab", lazy.screen.next_group()),
        Key(["mod1", "shift"], "Tab", lazy.screen.prev_group()),

# MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND STAY ON WORKSPACE
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
# MOVE WINDOW TO SELECTED WORKSPACE 1-10 AND FOLLOW MOVED WINDOW TO WORKSPACE
        Key([mod, "mod1"], i.name, lazy.window.togroup(i.name) , lazy.group[i.name].toscreen()),
    ])

layouts = [
    layout.MonadTall(margin=2, border_width=2, border_focus="#5e81ac", border_normal="#4c566a"),
    #layout.Columns(border_focus_stack='#d75f5f'),
    layout.Max(),
    layout.Floating(margin=2, border_width=2, border_focus="#5e81ac", border_normal="#4c566a"),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

def init_layout_theme():
    return {"margin":2,
            "border_width":2,
            "border_focus": "#5e81ac",
            "border_normal": "#4c566a"
            }

layout_theme = init_layout_theme()

widget_defaults = dict(
    font='Hack',
    fontsize=12,
    padding=3,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.CurrentLayout(foreground="#887ef2", background="#3e3773"),
                widget.GroupBox(active='887ef2', background="#3e3773", inactive= '131124', ),
                widget.Prompt(foreground="#887ef2", background="#3e3773"),
                widget.WindowName(foreground="#33028F", background="#887ef2"),
                widget.Chord(
                    chords_colors={
                        'launch': ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.Clock(format='%m-%d-%Y %a %I:%M %p', foreground="#887ef2", background="#3e3773"),
                widget.Systray(foreground="#887ef2", background="#3e3773"),
            ],
            24,
        ),
    ),
                        Screen(
        top=bar.Bar(
            [
                widget.CurrentLayout(foreground="#887ef2", background="#3e3773"),
                widget.GroupBox(active='887ef2', background="#3e3773", inactive= '131124', ),
                widget.Prompt(foreground="#887ef2", background="#3e3773"),
                widget.WindowName(foreground="#33028F", background="#887ef2"),
                widget.Chord(
                    chords_colors={
                        'launch': ("#ff0000", "#ffffff"),
                    },
                    name_transform=lambda name: name.upper(),
                ),
                widget.TextBox(font="Hack", text=" 5.17.5-zen1-1-zen", fontsize = 12, foreground = '887ef2', background = '3e3773',),
                widget.Memory(font="Hack", format = 'RAM:{MemUsed: .0f}MB/{MemTotal: .0f}MB', update_interval = 1, fontsize = 12, foreground = '887ef2', background = '3e3773',),
                widget.Clock(format='%m-%d-%Y %a %I:%M %p', foreground="#887ef2", background="#3e3773"),
                widget.QuickExit(default_text='[ Logout ]', foreground="#887ef2", background="#3e3773"),
            ],
            24,
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None  # WARNING: this is deprecated and will be removed soon
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
    Match(title='Flashplayer'),
    Match(title='kshutdown'),
])
auto_fullscreen = True
focus_on_window_activation = "smart"

@hook.subscribe.startup_once
def start_once():
    home = os.path.expanduser('~/.config/qtile/scripts/autostart.sh')
    subprocess.call([home])


# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"
