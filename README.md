My Qtile Config requires:<br>
psutil

My autostart requires:<br>
feh with Xinerama support enabled (use "man feh" to check)<br>
picom<br>
volumeicon applet<br>
lxqt-policykit-agent<br>
lxqt-notificationd <br>

Besides the little documentation above. I won't provide further<br>
documentation. It's up the user (you) to make changes to what they<br>
(you) don't want.<br>

Goto to the [Qtile wiki](https://docs.qtile.org/en/latest/index.html) for further documentation.
